import React from 'react'
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from "react-router-dom";

// add component
import { Authpg, Notepg, Editnot } from './pages'
import Header from "./component/Header"

function App() {
  return (
    <>
    <Header/>
      <div className="container">

        <Router>
          <Switch>
            <Route exact path="/auth" component={Authpg} />
            <Route exact path="/notepg" component={Notepg} />
            <Route exact path="/editnot" component={Editnot} />
            <Route exact path="/editnot/:editId" component={Editnot} />
            <Redirect to="/auth" />
          </Switch>
        </Router>

      </div>
    </>
  );
}

export default App;
