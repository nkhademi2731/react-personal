import React from 'react'
import { Login, Register } from '../component'
export default function Authpg() {
    return (
        <>
            <div className="row justify-content-between my-5">
                <div className="col-md-4">
                    <Login />
                </div>
                <div className="col-md-6">
                    <Register />
                </div>
            </div>
        </>
    )
}
